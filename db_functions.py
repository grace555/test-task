import MySQLdb
import csv
import datetime
import time


def create_connection(user, passwd):
    return MySQLdb.connect(host='localhost',
            user=user, passwd=passwd)


def create_new_db(db_name, connection, cursor):
    drop_db(db_name, connection, cursor)
    cursor.execute("CREATE DATABASE " + db_name)
    connection.select_db(db_name)
    create_table(connection, cursor)
    connection.commit()


def drop_db(db_name, connection, cursor):
    cursor.execute ("DROP DATABASE IF EXISTS " + db_name)
    connection.commit()


def create_table(connection, cursor):
    cursor.execute('''
        CREATE TABLE Workers(
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `name` CHAR(30) NOT NULL,
          `surname` CHAR(30) NOT NULL,
          `company` CHAR(30) NOT NULL,
          `position` CHAR(30) NOT NULL,
          `date_of_birth` DATE NOT NULL,
          `url` CHAR(255) NOT NULL,
          PRIMARY KEY (`id`)
          )
          AUTO_INCREMENT=1
        ''')
    connection.commit()


def insert_csv_information(connection, cursor, csv_file_name):
    sql = '''INSERT INTO `Workers` (`name`, `surname`, `company`, `position`,
                  `date_of_birth`, `url`) VALUES(%s, %s, %s, %s, %s, %s)'''
    with open(csv_file_name) as csv_file:
        csv_data = csv.reader(csv_file)
        for row in csv_data:
            cursor.execute(sql,row)
    connection.commit()
