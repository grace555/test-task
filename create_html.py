import urllib2
import cStringIO
from PIL import ImageDraw2, Image
import os
import datetime


style = '''table {border-collapse: collapse;
    border: 0;}
td {padding: 2px;
    padding-right: 30px;
    border: 0;}
caption {text-align: left;
    font-size: 20px}
text{  font-size: 14px;}'''


def create_html():
    return '<!DOCTYPE html><html><head><title>database</title><style>' + style + '</style></head><body>'


def add_heading(company, html, amount):
    return html + '<table border="1"><caption>' + company + ' <text>[{}'.format(amount) + 'worker(s)]</text>' + '</caption>'


def add_row(row,html, client_name):
    download_image(row[6], client_name)
    row = list(row)
    row[5] = datetime.datetime.strftime(row[5], "%Y-%m-%d")
    del row[6]
    del row[3]
    del row[0]
    html += '<tr>'
    for each in row:
        html += '<td>' + each + '</td>'
    return  html + '<td><img src=\'{}\'/><td></tr>'.format('test.png')

def add_footer( html):
    return html + '</table><p><br><br></p>'


def download_image(img_url, client_name):
    file = cStringIO.StringIO(urllib2.urlopen(img_url).read())
    im = Image.open(file)
    im = image_resize(im)
    watermark(im, client_name)
    im.save('test.png')


def image_resize(image):
    width, height = image.size
    if height > width:
        index = 50.0 / height
        width *= index
        image = image.resize((int(round(width, 0)), 50), Image.ANTIALIAS)
    else:
        index = 50.0 / width
        height *= index
        image = image.resize((50, height), Image.ANTIALIAS)
    return image


def watermark(im, client_name):
    draw = ImageDraw2.Draw(im)
    font = ImageDraw2.Font('white', 'RobotoCondensed-Bold.ttf',10)
    draw.text((0, 40), client_name, font=font)


def create_file(filename, html):
    html += '</body></html>'
    f = open(filename, 'w')
    f.write(html)
    f.close()