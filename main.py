from db_functions import *
from create_html import *
import sys, argparse


def create_db(user, passwd, db_name, csv_file_name, client_name):
    try:
        connection= create_connection(user, passwd)
        cursor = connection.cursor()
        create_new_db(db_name, connection, cursor)
        try:
            insert_csv_information(connection, cursor, csv_file_name)
        except:
            print('Prombems importing data. \t Please, check ur csv file')
        cursor.execute("""SELECT `company` FROM Workers GROUP BY `company`""")
        row = cursor.fetchall()
        html = create_html()
        for r in row:
            company = list(r)
            cursor.execute("""SELECT * FROM Workers WHERE `company` = %s""",r)
            works = cursor.fetchall()
            html = add_heading(company[0], html, len(works))
            for w in works:
                html = add_row(w,html, client_name)
                print(w)
            html = add_footer(html)
        create_file('test.html', html)
    except:
        print('Problems creation connection. \t Please, check ur Mysql user and batabase name')


parser = argparse.ArgumentParser()
parser.add_argument('-u', '--user', help="Mysql-user username")
parser.add_argument('-p', '--passwd', help="Mysql-user password")
parser.add_argument('-db', '--database', help="Result db name")
parser.add_argument('-f', '--file', help="your csv file")
parser.add_argument('-n', '--name', help="your name")
args = parser.parse_args()

csv_file_name = args.file
client_name = args.name
user = args.user
passwd = args.passwd
db_name = args.database

create_db(user, passwd, db_name, csv_file_name, client_name)
